## INF214-v20 - Search engine construction

#### Main task: the creation of an *inverted index* for a collection of documents

- **Keywords:**  Information Retrieval, Concurrency HashMap
- **Learning outcomes:**
  - The *Inverted index* data structure
  - Concurrent *HashMap* data structure

### Description

 We want to make searches of a word or a *list of words* in a document collection. One approach used in many search engines is the usage of the so-called  *inverted index*. Such a thing allow us to obtain the list of documents associated with each word and create a unique list with the results of the search. We expect you to do the following tasks.

0. Create a private repository for your project on https://git.app.uib.no/.
1. Implement Java classes for text indexer.indexing as specified below.
2. Write a README file with a description of your project and the following information.
3. Times of execution for all implementations, using different dataset sizes (1,100,1000, 5000, 10.000, and more).
4. Specify the machine architecture used in the experiment. For example:
   - MacBook Pro (Retina, 15-inch, Mid 2015).
     - Processor: 2,2 GHz Quad-Core Intel Core i7.
     - Memory: 16 GB 1600 MHz DDR3
5. Report the speed-up one can get with the concurrent programming, and give your conclusions.
6. Tag the release of your submission: https://git-scm.com/book/en/v2/Git-Basics-Tagging
7. Finally, make sure of giving access in GitLab to the reviewers  (@Brigt.Havardstun, @einarkjellback, @ErikHys,  @jonaprieto, @Mikhail.Barash).

#### Background

The *Inverted index* is a common *data structure* used to speed up the searches of text in a collection of documents. It stores all the words of the document collection and a list of the documents that contain that word.

1. Parse all the documents of the collection

2. Incrementally construct the index:

   - For each document we perform the following operations:

     1. Extract the significant words of that document:

        - Delete the most common words also called **stop words** e.g: "and, or, the, ..."
        - *Optional*: apply a stemming algorithm.

     2. Add the words to the index:

        - If a word exists in the index, we add the document to the list of documents associated with that word.

        - If a word doesn't exist, add the word to the list of words of the index and associate the document to that word.

   *Optional*:  Besides the basic information like the document occurrences, each indexed word may contain information as:

   - Term frequency of the word in the document
   - Relevance scores based on the whole corpus or per document.

### Implementation in Java

#### Warming up #1

We have created a template for your convenience with datasets of documents for testing. We encourage you to follow the same file structure and name conventions given below to make our revision process more efficient. Don't forget to include in your `README` file the instructions to run your project in case it differs from the specification below.

```bash
$ git clone --recurse-submodules https://git.app.uib.no/inf214h20/search-engine-template
$ cd search-engine-template
$ tree -L 3
.
├── README.md
├── datasets
│   └── text
│       ├── big
│       └── small
├── search-engine.iml
└── src
    └── indexer
        ├── ConcurrentIndexing.java
        ├── Document.java
        ├── DocumentParser.java
        ├── Main.java
        ├── MultipleConcurrentIndexing.java
        └── SerialIndexing.java
```

---

### Java classes description

The following descriptions will give you an idea of what you need to implement in Java to achieve the goal of this practice. For a better overview please review the template code mentioned above.

#### Warming up #2

- The `Document` class stores the list of words contained in the document.
  -  Implement getters and setters for the following data:
     - name of the file, `fileName`, and
     - vocabulary as a map called `voc` with:
       - key as words in the document,
       - value as the frequency of the word in the document.
- The `DocumentParse` class converts a document stored in a file in a document object. You must implement the following methods:
  - `public Map<String, Integer> parse(String route)`
  - `private void parseLine(String line, Map<String, Integer> voc) `
- The `SerialIndexing` class:

  - reads all the documents from a given directory path
  - gets their vocabulary, and
  - constructs the inverted index in an incremental way, for which, you must implement a method to update the index, call it `updateInvertedIndex`.

#### Warming up #3

- The `ConcurrentIndexing`class is the main class in this laboratory and it should create and launch all the components, waits for its finalization, and return the final execution time in the console to process one document per each concurrent task. This class is the concurrent version of the serial indexing, so please check above the description. We expect you to investigate the Executor Java framework to implement the solution. This framework provides facilities for creating and management of a lot of concurrent tasks. The main classes in this framework, available in `java.util.concurrent`, are the following.

  - The `Executors` class and its interface `Executor`,
  
  * the  `ThreadPoolExecutor` class, 
  * the `Callable` interface vs the `Runnable` interface,
  * the `Future` interface,
  * the `ExecutorCompletionService` class and its `CompletionService` interface, and
  * the concurrent data structure implementation of a hash map,  the `ConcurrentHashMap` class.

#### Warming up #4

- `MultipleConcurrentIndexing` class only differs from ConcurrentIndexing class in that a task can process more than one document at a time. The number of documents is a parameter for the constructor of this class. As before, we recommend checking the template code we provide above to give you an idea of what we expect you implement. Nevertheless, you are free to follow your approach and the best practices for programming.

---

### Testing with real data

For testing purposes, in the repository above, we have included a submodule called  `datasets` (https://git.app.uib.no/inf214h20/datasets). You will find in that directory many texts from Wikipedia (https://en.wikipedia.org/wiki/Wikipedia:Database_download, http://kopiwiki.dsd.sztaki.hu/). However, you might want to use other sources. For example, you can download books from the Project Gutenberg (https://www.gutenberg.org/) by running the following command:

```bash
$ wget -np -nd -H -w 2 -m http://www.gutenberg.org/robot/harvest\?filetypes\[\]\=txt\&langs\[\]\=en \
--referer="http://www.google.com" \
--user-agent="Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6" \
--header="Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5" \
--header="Accept-Language: en-us,en;q=0.5" \
--header="Accept-Encoding: gzip,deflate" \
--header="Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7" \
--header="Keep-Alive: 300"
```

---