package indexer;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import java.util.logging.*;
import static java.util.logging.Level.INFO;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class.getCanonicalName());


    public static void main(String[] args) {
        File dataset = new File("datasets/text/small");
        File[] files = dataset.listFiles();
        assert files != null;

        logger.log(INFO, "Path: {0}\nNumber of files: {1}", new Object[]{dataset, files.length});

        String query = "book";
        SerialIndexing serial = new SerialIndexing();
        serial.index(files);
        logger.log(INFO, "Serial method: \t{0}ms" , serial.getExecutionTime());

        ConcurrentIndexing concurrencyMod1 = new ConcurrentIndexing();
        concurrencyMod1.index(files);

        logger.log(INFO, "Conc. method 1:\t{0}ms" , concurrencyMod1.getExecutionTime());
        int numberDocsPerTask = 5000;
        MultipleConcurrentIndexing concurrencyMod2 = new MultipleConcurrentIndexing(numberDocsPerTask);
        concurrencyMod2.index(files);

        logger.log(INFO, "Conc. method 2 :\t{0}ms" , concurrencyMod2.getExecutionTime());

        Set<String> ans1 = new HashSet<>(Arrays.asList(serial.searchQuery(query)));
        Set<String> ans2 = new HashSet<>(Arrays.asList(concurrencyMod1.searchQuery(query)));
        Set<String> ans3 = new HashSet<>(Arrays.asList(concurrencyMod2.searchQuery(query)));
        assert ans1.equals(ans2) && ans2.equals(ans3);

        logger.log(INFO, "Query: {0}", query);
        logger.log(INFO, "Documents: {0}" , serial.searchQuery(query).length);

    }
}